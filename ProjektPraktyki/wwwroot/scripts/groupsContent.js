﻿if (getCookie('selectedGroupIndex') == null)
    setCookie('selectedGroupIndex', -1);

class Group {
    constructor(id, name) {
        this.id = id;
        this.name = name;
        this.addresses = [];
        this.loaded = false;
    }
}

//  Funkcja do załadowania adresów z serwera i wyświetlenia ich.
function loadAddresses(index)
{
    //  Nieprawidłowy index:
    if (index >= modelData[MODEL_GROUPS].length || index == -1)
        return refreshAddresses(-1);

    //  Adresy już załadowane, po prostu odśwież:
    let group = modelData[MODEL_GROUPS][index];
    if (group.loaded) {
        return refreshAddresses(index);
    }

    //  Wczytaj adresy z serwera i odśwież:
    let data = getPostData('GetAddressesPost', group.id);
    group.addresses = [];
    data.then(function (data) {
        if (data.result) {
            let addresses = data.addresses;
            for (let i = 0, end = addresses.length; i < end; ++i)
                group.addresses.push(addresses[i]);

            group.loaded = true;
            refreshAddresses(index);
        }
        else
            refreshAddresses(-1);
    });
}

//  Funkcja do usuwania adresu.
function deleteAddress(address)
{
    let index = getCookie('selectedGroupIndex');
    let group = modelData[MODEL_GROUPS][index];

    let data = getPostData('DeleteAddressPost', { address: address, groupId: group.id });
    data.then(function (data)
    {
        if (data.result)
        {
            let addresses = group.addresses;
            for (let i = 0, len = addresses.length; i < len; ++i) {
                if (addresses[i] == address) {
                    addresses.splice(i, 1);
                    refreshAddresses(index);
                    break;
                }
            }

            /*
            group.addresses.push(address);
            document.getElementById('addressesContainerList').innerHTML += getAddressText(address);
            input.value = '';*/
        }
    });
}

//  Funkcja do zresetowania wyświetlanych adresów.
function resetAddresses() {
    document.getElementById('groupName').innerText = 'Twoje grupy';
    document.getElementById('addressesContainerList').innerHTML = '';

    document.getElementById('addAddressForm').innerHTML = `
    <input id = "addAddressInput" type = "text" name = "email" placeholder = "Wprowadź email" required>
    <button type="submit" disabled>Dodaj</button>`;
}

//  Odświeżanie wyświetlanych adresów po wybraniu grupy.
function refreshAddresses(index) {
    if (index == -1)
        return resetAddresses();

    let addresses = modelData[MODEL_GROUPS][index].addresses;
    let text = '';
    for (let i = 0, end = addresses.length; i < end; ++i)
        text += getAddressText(addresses[i]);

    document.getElementById('groupName').innerText = modelData[MODEL_GROUPS][index].name;
    document.getElementById('addressesContainerList').innerHTML = text;

    document.getElementById('addAddressForm').innerHTML = `
    <input id = "addAddressInput" type = "text" name = "email" placeholder = "Wprowadź email" required>
    <button type="submit">Dodaj</button>`;
}

//  Przełączanie pomiędzy grupami.
function toggleGroup(index) {
    if (getCookie('selectedGroupIndex') == index)
        return;

    setCookie('selectedGroupIndex', index);
    loadAddresses(index);
}

//  Funkcja do usuwania grupy.
function deleteGroup(index)
{
    let data = getPostData('DeleteGroupsPost', modelData[MODEL_GROUPS][index].id);
    data.then(function (data)
    {
        if (data == true) {
            if (getCookie('selectedGroupIndex') == index)
                setCookie('selectedGroupIndex', -1);

            modelData[MODEL_GROUPS].splice(index, 1);
            loadGroups();
        }
    });
}

function getGroupText(index, name) {
    return `<div class="groupsContainerVertical">
        <button class="groupsContainerVerticalPart" onClick="toggleGroup(${index})">${name}</button>
        <button class="groupsContainerVerticalPart" onClick="deleteGroup(${index})">Potwierdź</button>
    </div>`;
}

function getAddressText(address) {
    return `<div class="groupsContainerVertical">
        <span class="groupsContainerVerticalPart">${address}</span>
        <button class="groupsContainerVerticalPart" onClick="deleteAddress('${address}')">Potwierdź</button>
    </div>`;
}

//  Funkcja do odświeżenia wyświetlanych grup na podstawie załadowanych już danych.
function refreshGroups() {
    let text = '';

    for (let i = 0, len = modelData[MODEL_GROUPS].length; i < len; ++i) {
        let group = modelData[MODEL_GROUPS][i];
        text += getGroupText(i, group.name);
    }

    document.getElementById('groupsContainerList').innerHTML = text;
    loadAddresses(getCookie('selectedGroupIndex'));
}

//  Główna funkcja do wczytania struktury HTML i załadowania grup z serwera.
function loadGroups()
{
    //  Tworzenie struktury HTML:
    document.getElementById('contentContainer').innerHTML = `
    <div id="groupsContainer" class="groupsContainer">
        <div class="groupsContainerHorizontal groupsContainerLeft">
            <div class="groupsHeader">
                <h2 id="groupName">Twoje adresy</h2>
                <div class="groupsContainerVertical">
                    <span class="groupsContainerVerticalPart">Nazwa</span>
                    <span class="groupsContainerVerticalPart">Usuń</span>
                </div>
            </div>

            <div id="addressesContainerList" class="scrollbarContainer groupsContainerList"></div>

            <div class="groupsContainerTools">
                <div class="groupsContainerVertical">
                    <form id="addAddressForm" class="groupsContainerVerticalPart groupsContainerForm">
                        <input id="addAddressInput" type="text" name="email" placeholder="Wprowadź email" required>
                        <button type="submit" disabled>Dodaj</button>
                    </form>

                    <div class="groupsContainerVerticalPart groupsContainerForm">
                        <h2>Opcje</h2>
                        <button type="button">Importuj</button>
                        <button type="button">Eksportuj</button>
                    </div>
                </div>
            </div>
        </div>

        <div class="groupsContainerHorizontal groupsContainerRight">
            <div class="groupsHeader">
                <h2>Twoje grupy</h2>
                <div class="groupsContainerVertical">
                    <span class="groupsContainerVerticalPart">Wybierz</span>
                    <span class="groupsContainerVerticalPart">Usuń</span>
                </div>
            </div>

            <div id="groupsContainerList" class="scrollbarContainer groupsContainerList"></div>

            <div class="groupsContainerTools">
                <form id="addGroupForm" class="groupsContainerVertical groupsContainerForm">
                    <input id="addGroupInput" type="text" name="groupName" placeholder="Wprowadź nazwę grupy" required>
                    <button type="submit">Dodaj</button>
                </form>
            </div>
        </div>
    </div>`;

    //  Ładowanie grup z serwera:
    if (!modelData[MODEL_GROUPS_LOADED]) {
        modelData[MODEL_GROUPS] = [];
        let data = getPostData('GetGroupsPost');

        data.then(function (data) {
            for (let i = 0, groupsLen = data.groups.length; i < groupsLen; ++i) {
                let group = data.groups[i];
                modelData[MODEL_GROUPS].push(new Group(group.id, group.name));
            }

            modelData[MODEL_GROUPS_LOADED] = true;
            refreshGroups();
        });
    }
    else
        refreshGroups();

    //  Obsługa dodawania nowej grupy:
    document.getElementById('addGroupForm').addEventListener('submit', function (event) {
        event.preventDefault();

        let input = document.getElementById('addGroupInput');
        let data = getPostData('AddGroupsPost', input.value);
        data.then(function (data) {
            if (data.result) {
                let id = data.id;
                let name = data.name;
                let index = modelData[MODEL_GROUPS].length;

                modelData[MODEL_GROUPS].push(new Group(id, name));
                input.value = '';

                document.getElementById('groupsContainerList').innerHTML += getGroupText(index, name);
            }
        });
    });

    //  Obsługa dodawania nowego adresu:
    document.getElementById('addAddressForm').addEventListener('submit', function (event) {
        event.preventDefault();

        let input = document.getElementById('addAddressInput');
        let address = input.value;
        let index = getCookie('selectedGroupIndex');
        let group = modelData[MODEL_GROUPS][index];

        let data = getPostData('AddAddressPost', { address: address, groupId: group.id });
        data.then(function (data) {
            if (data.result) {
                group.addresses.push(address);
                document.getElementById('addressesContainerList').innerHTML += getAddressText(address);
                input.value = '';
            }
        });
    });
}
