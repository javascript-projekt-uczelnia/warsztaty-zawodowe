﻿//----------------------------------------------
//  Stałe:
//----------------------------------------------
const MODEL_IS_LOGGED = 0;
const MODEL_NICKNAME = 1;
const MODEL_LOGIN_ERROR_MESSAGE = 2;
const MODEL_GROUPS_LOADED = 3;
const MODEL_GROUPS = 4;

let modelData = {};

//  logging.js
const LOGGING_STANDARD = '0';
const LOGGING_PERSON = '1';
const LOGGING_COMPANY = '2';

const LOGGING_ERROR_NONE = '0';
const LOGGING_ERROR_ACCOUNT_NOT_FOUND = '1';
const LOGGING_ERROR_WRONG_PASSWORD = '2';
const LOGGING_ERROR_WRONG_REPEATED_PASSWORD = '3';
const LOGGING_ERROR_NO_EMAIL_AND_PHONE_NUMBER = '4';
const LOGGING_ERROR_NICKNAME_ALREADY_EXISTS = '5';

//  primaryOptions.js
const PRIMARY_OPTION_NONE = '0';
const PRIMARY_OPTION_ACCOUNT = '1';
const PRIMARY_OPTION_MAILING = '2';
const PRIMARY_OPTION_MANAGMENT = '3';

//  secondaryOptions.js
const SECONDARY_OPTION_NONE = '0';

const SECONDARY_OPTION_ACCOUNT_PASSWORD = '1';
const SECONDARY_OPTION_ACCOUNT_DATA = '2';
const SECONDARY_OPTION_ACCOUNT_STATISTICS = '3';

const SECONDARY_OPTION_MAILING_MESSAGE = '4';
const SECONDARY_OPTION_MAILING_DRAFT = '5';

const SECONDARY_OPTION_MANAGMENT_GROUPS = '6';
const SECONDARY_OPTION_MANAGMENT_SCHEMES = '7';

//----------------------------------------------
//  Cookies i narzędzia:
//----------------------------------------------
function getCookie(key) {
    const value = document.cookie.match(new RegExp('(^|;)[\\s]*' + key + '=([^;]*)'));
    return value ? value[2] : null;
}

function setCookie(key, value) {
    document.cookie = key + "=" + value + "; expires=0; path=/";
}

function deleteCookie(key) {
    document.cookie = key + "=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
}

function isStringNotEmpty(str) {
    return str.trim().length > 0;
}

//----------------------------------------------
//  Funkcje do przesyłania danych metodą post:
//----------------------------------------------
async function sendPost(funcName) {
    try {
        let url = '/Home/' + funcName;
        const response = await fetch(url, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            }
        });

        if (response.ok) {
            return true;
        } else {
            return false;
        }
    } catch (error) {
        console.error('Błąd:', error);
        return false;
    }
}

async function getPostData(funcName, data = null) {
    try {
        let url = '/Home/' + funcName;
        const response = await fetch(url, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        });

        if (response.ok) {
            return await response.json();
        } else {
            return null;
        }
    } catch (error) {
        return null;
    }
}

//----------------------------------------------
//  Funkcje do wczytywania zawartości:
//----------------------------------------------
function loadMain() {
    //  Niezalogowany, wczytaj panel logowania.
    let mainHeader = document.getElementById('mainHeader');
    let mainHeaderText = `
        <div>
            <img src="images/pwsteLogo.png" class="logoPwste" />
            <h1 class="headerStartText">Aplikacja Mailingowa</h1>
        </div>`;

    let mainContainer = document.getElementById('mainContainer');
    if (modelData[MODEL_IS_LOGGED] == 0) {
        mainHeader.innerHTML = mainHeaderText;
        mainContainer.innerHTML = `<div id="loggingContainer" class="loggingContainer scrollbarContainer"></div>`;
        loadLogging(getCookie('loggingId'));
    }
    else {
        mainHeaderText += `
            <nav id="primaryNavigation" class="primaryNavigation">
                <button type="button" onClick="togglePrimaryOption(PRIMARY_OPTION_ACCOUNT)">Konto</button>
                <button type="button" onClick="togglePrimaryOption(PRIMARY_OPTION_MAILING)">Mailing</button>
                <button type="button" onClick="togglePrimaryOption(PRIMARY_OPTION_MANAGMENT)">Zarządzanie</button>
            </nav>

            <div class="logOutContainer">
                <h2>${modelData[MODEL_NICKNAME]}</h2>
                <form method="post" action="/Home/logOutForm">
                    <button type="submit">Wyloguj</button>
                </form>
            </div>`;

        mainHeader.innerHTML = mainHeaderText;
        mainContainer.innerHTML = `
            <nav id="secondaryNavigation" class="secondaryNavigation"></nav>
            <div id="contentContainer" class="contentContainer"></div>`;

        loadPrimaryOption(getCookie('primaryOptionId'));
        loadSecondaryOption(getCookie('secondaryOptionId'));
    }
}