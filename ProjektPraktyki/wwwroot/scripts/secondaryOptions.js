﻿if (getCookie('secondaryOptionId') == null)
    setCookie('secondaryOptionId', SECONDARY_OPTION_NONE);

function toggleSecondaryOption(id) {
    if (getCookie('secondaryOptionId') == id)
        return;

    setCookie('secondaryOptionId', id);
    loadSecondaryOption(id);
}

function loadSecondaryOption(id)
{
    let contentContainer = document.getElementById('contentContainer');
    switch (id) {
        //  Account:
        case SECONDARY_OPTION_ACCOUNT_PASSWORD:
            {
                contentContainer.innerHTML = '';
            }
            break;

        case SECONDARY_OPTION_ACCOUNT_DATA:
            {
                contentContainer.innerHTML = '';
            }
            break;

        case SECONDARY_OPTION_ACCOUNT_STATISTICS:
            {
                contentContainer.innerHTML = '';
            }
            break;

        //  Mailing:
        case SECONDARY_OPTION_MAILING_MESSAGE:
            loadMailing();
            break;

        case SECONDARY_OPTION_MAILING_DRAFT:
            {
                contentContainer.innerHTML = '';
            }
            break;

        //  Managment:
        case SECONDARY_OPTION_MANAGMENT_GROUPS:
                loadGroups();
            break;

        case SECONDARY_OPTION_MANAGMENT_SCHEMES:
            {
                contentContainer.innerHTML = '';
            }
            break;

        default:
                contentContainer.innerHTML = '';
            break;
    }
}

