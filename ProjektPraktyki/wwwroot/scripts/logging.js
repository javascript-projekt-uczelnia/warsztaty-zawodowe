﻿if (getCookie('loggingId') == null)
    setCookie('loggingId', LOGGING_STANDARD);

function loadLoggingErrorMessage(id) {
    let element = document.getElementById('loggingErrorMessage');
    switch (id) {
        case LOGGING_ERROR_ACCOUNT_NOT_FOUND:
            element.innerText = 'Podany użytkownik nie istnieje!';
            break;

        case LOGGING_ERROR_WRONG_PASSWORD:
            element.innerText = 'Podano nieprawidłowe hasło!';
            break;

        case LOGGING_ERROR_WRONG_REPEATED_PASSWORD:
            element.innerText = 'Podane hasło nie zgadza się z powtórzonym!';
            break;

        case LOGGING_ERROR_NO_EMAIL_AND_PHONE_NUMBER:
            element.innerText = 'Podaj numer telefonu lub email!';
            break;

        case LOGGING_ERROR_NICKNAME_ALREADY_EXISTS:
            element.innerText = 'Podana nazwa użytkownika jest już zajęta!';
            break;

        default:
            element.innerText = '';
            break;
    }
}

function toggleLogging(id) {
    if (getCookie('loggingId') == id)
        return;

    setCookie('loggingId', id);
    modelData[MODEL_LOGIN_ERROR_MESSAGE] = LOGGING_ERROR_NONE;
    loadLogging(id);
}

function loadLogging(id) {
    let text = '';
    switch (id) {
        case LOGGING_STANDARD:
            {
                text = `
                    <div>
                        <h1>Logowanie</h1>
                        <p>Wprowadź nazwę użytkownika i hasło!</p>
                    </div>

                    <form method="post" action="/Home/logInForm">
                        <div>
                            <input type="text" name="nickname" placeholder="Nazwa" required>
                            <input type="password" name="password" placeholder="Hasło" required>
                            <p id="loggingErrorMessage" class="loggingErrorMessage"></p>
                        </div>

                        <div>
                            <button type="submit">Zaloguj</button>
                            <button type="button" class="loggingToggleButton" onclick="toggleLogging(LOGGING_PERSON)">Nie masz konta?</button>
                        </div>
                    </form>`;
            }
            break;

        case LOGGING_PERSON:
            {
                text = `
                    <div>
                        <h1>Rejestracja</h1>
                        <button type="button" class="loggingToggleButton" onclick="toggleLogging(LOGGING_COMPANY)">Rejestrujesz się jako osoba!</button>
                    </div>

                        <form id="registrationForm" method="post" action="/Home/RegisterPersonForm">
                        <div>
                            <p>Wprowadź dane logowania!</p>
                            <input type="text" name="nickname" placeholder="Login" required>
                            <input type="password" name="password" placeholder="Hasło" required>
                            <input type="password" name="repeatPassword" placeholder="Powtórz hasło" required>
                        </div>

                        <div>
                            <p>Wprowadź dane osobowe!</p>
                            <input type="text" name="name" placeholder="Imię" required>
                            <input type="text" name="surname" placeholder="Nazwisko" required>
                        </div>

                        <div>
                            <p>Wprowadź dane kontaktowe!</p>
                            <input type="text" name="email" placeholder="email">
                            <input type="tel" name="phoneNumber" pattern="[0-9]{9}|[0-9]{3}-[0-9]{3}-[0-9]{3}|[0-9]{2}-[0-9]{2}-[0-9]{2}-[0-9]{3}" placeholder="telefon">
                            <p id="loggingErrorMessage" class="loggingErrorMessage"></p>
                        </div>

                        <div>
                            <button type="submit">Utwórz konto</button>
                            <button type="button" class="loggingToggleButton" onclick="toggleLogging(LOGGING_STANDARD)">Masz już konto?</button>
                        </div>
                    </form>`;
            }
            break;

        default:
            {
                text = `
                    <div>
                        <h1>Rejestracja</h1>
                        <button type="button" class="loggingToggleButton" onclick="toggleLogging(LOGGING_PERSON)">Rejestrujesz się jako firma!</button>
                    </div>

                    <form id="registrationForm" method="post" action="/Home/RegisterCompanyForm">
                        <div>
                            <p>Wprowadź dane logowania!</p>
                            <input type="text" name="nickname" placeholder="Login" required>
                            <input type="password" name="password" placeholder="Hasło" required>
                            <input type="password" name="repeatPassword" placeholder="Powtórz hasło" required>
                        </div>

                        <div>
                            <p>Wprowadź dane!</p>
                            <input type="text" name="name" placeholder="Nazwa" required>
                            <input type="text" name="email" placeholder="email">
                            <input type="tel" name="phoneNumber" pattern="[0-9]{9}|[0-9]{3}-[0-9]{3}-[0-9]{3}|[0-9]{2}-[0-9]{2}-[0-9]{2}-[0-9]{3}" placeholder="telefon">
                            <p id="loggingErrorMessage" class="loggingErrorMessage"></p>
                        </div>

                        <div>
                            <button type="submit">Utwórz konto</button>
                            <button type="button" class="loggingToggleButton" onclick="toggleLogging(LOGGING_STANDARD)">Masz już konto?</button>
                        </div>
                    </form>
                    `;
            }
            break;
    }

    let loggingContainer = document.getElementById('loggingContainer');
    loggingContainer.innerHTML = text;
    loadLoggingErrorMessage(modelData[MODEL_LOGIN_ERROR_MESSAGE]);

    let registrationForm = document.getElementById('registrationForm');
    if (registrationForm != null) {
        registrationForm.addEventListener('submit', function (event) {
            let password = document.querySelector('input[name="password"]').value;
            let repeatPassword = document.querySelector('input[name="repeatPassword"]').value;

            if (password !== repeatPassword) {
                event.preventDefault();
                loadLoggingErrorMessage(LOGGING_ERROR_WRONG_REPEATED_PASSWORD);
                return;
            }

            let email = document.querySelector('input[name="email"]').value;
            let phoneNumber = document.querySelector('input[name="phoneNumber"]').value;

            if (email.length === 0 && phoneNumber.length === 0) {
                event.preventDefault();
                loadLoggingErrorMessage(LOGGING_ERROR_NO_EMAIL_AND_PHONE_NUMBER);
                return;
            }
        });
    }
}