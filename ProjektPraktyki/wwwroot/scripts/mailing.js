﻿function refreshMailingGroups() {
    let text = '<option value="-1" disabled selected hidden >Wybierz grupę</option>';
    for (let i = 0, len = modelData[MODEL_GROUPS].length; i < len; ++i) {
        let group = modelData[MODEL_GROUPS][i];
        text += `<option value="${group.id}">${group.name}</option>`;
    }

    document.getElementById('mailingGroupsSelect').innerHTML = text;
}

function loadMailing() {
    document.getElementById('contentContainer').innerHTML = `
    <form id="sendMailForm" class="mailContainer scrollbarContainer">
        <select id="mailingGroupsSelect" class="mailContainerOption" required>
            <option value="-1" disabled selected hidden>Wybierz grupę</option>
        </select>

        <input type="text" id="mailingTitleInput" placeholder="Wprowadź tytuł" required>
        <textarea id="mailingBodyInput" placeholder="Wprowadź treść" rows="10" required></textarea>

        <input type="text" id="mailingEmailInput" placeholder="Wprowadź email" required>
        <input type="password" id="mailingPasswordInput" placeholder="Wprowadź hasło" required>
    
        <p id="mailingErrorMessage"></p>
        <button type="submit">Wyślij</button>
    </form>`;

    

    //  Ładowanie grup z serwera:
    if (!modelData[MODEL_GROUPS_LOADED]) {
        modelData[MODEL_GROUPS] = [];
        let data = getPostData('GetGroupsPost');

        data.then(function (data) {
            for (let i = 0, groupsLen = data.groups.length; i < groupsLen; ++i) {
                let group = data.groups[i];
                modelData[MODEL_GROUPS].push(new Group(group.id, group.name));
            }

            modelData[MODEL_GROUPS_LOADED] = true;
            refreshMailingGroups();
        });
    }
    else
        refreshMailingGroups();

    //  Obsługa formularza:
    let sendMailForm = document.getElementById('sendMailForm');
    let mailingErrorMessage = document.getElementById('mailingErrorMessage');
    sendMailForm.addEventListener('submit', function (event) {
        event.preventDefault();

        let groupId = document.getElementById('mailingGroupsSelect').value;
        if (groupId == -1) {
            mailingErrorMessage.innerText = 'Nie wybrano grupy!';
            return;
        }

        let title = document.getElementById('mailingTitleInput').value;
        let body = document.getElementById('mailingBodyInput').value;
        let email = document.getElementById('mailingEmailInput').value;
        let password = document.getElementById('mailingPasswordInput').value;

        console.log(groupId);
        console.log(title);
        console.log(body);
        console.log(email);
        console.log(password);

        let data = getPostData('sendMailPost', {
            groupId: groupId,
            title: title,
            body: body,
            email: email,
            password: password
        });

        data.then(function (data) {
            if (data.result) {
                mailingErrorMessage.innerText = 'Wysłano maile!';
            } else
                mailingErrorMessage.innerText = 'Wystąpił błąd!';

            sendMailForm.reset();
        });
    });
}