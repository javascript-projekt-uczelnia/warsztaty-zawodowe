﻿if (getCookie('primaryOptionId') == null)
    setCookie('primaryOptionId', PRIMARY_OPTION_NONE);

function togglePrimaryOption(id) {
    if (getCookie('primaryOptionId') == id)
        return;

    setCookie('primaryOptionId', id);
    loadPrimaryOption(id);
    toggleSecondaryOption(SECONDARY_OPTION_NONE);
}

function loadPrimaryOption(id)
{
    let secondaryNavigation = document.getElementById('secondaryNavigation');
    let text = ``;

    switch (id)
    {
        case PRIMARY_OPTION_ACCOUNT:
            {
                text += `
                <h2>Konto</h2>
                <button type="button" onClick="toggleSecondaryOption(SECONDARY_OPTION_ACCOUNT_PASSWORD)">Zmień hasło</button>
                <button type="button" onClick="toggleSecondaryOption(SECONDARY_OPTION_ACCOUNT_DATA)">Dane osobowe</button>
                <button type="button" onClick="toggleSecondaryOption(SECONDARY_OPTION_ACCOUNT_STATISTICS)">Statystyki</button>`;
            }
            break;

        case PRIMARY_OPTION_MAILING:
            {
                text += `
                <h2>Mailing</h2>
                <button type="button" onClick="toggleSecondaryOption(SECONDARY_OPTION_MAILING_MESSAGE)">Wyślij wiadomość</button>
                <button type="button" onClick="toggleSecondaryOption(SECONDARY_OPTION_MAILING_DRAFT)">Wersje robocze</button>`;
            }
            break;

        case PRIMARY_OPTION_MANAGMENT:
            {
                text += `
                <h2>Zarządzanie</h2>
                <button type="button" onClick="toggleSecondaryOption(SECONDARY_OPTION_MANAGMENT_GROUPS)">Grupy</button>
                <button type="button" onClick="toggleSecondaryOption(SECONDARY_OPTION_MANAGMENT_SCHEMES)">Schematy</button>`;
            }
            break;
    }

    secondaryNavigation.innerHTML = text;
}