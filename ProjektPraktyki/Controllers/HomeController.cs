﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using MySqlConnector;
using System.Collections.Generic;
using Microsoft.Extensions.Configuration;
using System.Data;
using System.Collections;
using System.Text.RegularExpressions;
using System.Reflection;
using Microsoft.AspNetCore.Http.HttpResults;
using System.Xml.Linq;
using System.Security.Cryptography;
using static System.Runtime.InteropServices.JavaScript.JSType;
using System.Text.Json;
using System.Net;
using System.Net.Mail;
using FluentEmail.Core.Models;


namespace AplikacjaWWW.Controllers
{
    public enum ContentOptions
    {
        None = 0,
        Groups
    }

    public class Group
    {
        public int id = -1;
        public string name = "";
        public List<string> addresses = new List<string>();

        public Group(int id, string name)
        {
            this.id = id;
            this.name = name;
        }
    }

    public class DataModel
    {
        public int isLogged = 0;
        public string nickname = "";

        public int loggingErrorMessage = HomeController.LOGGING_ERROR_MESSAGE_NONE;
        public ContentOptions contentOptions = ContentOptions.None;
    }

    public class HomeController : Controller
    {
        public void SendEmail(string senderEmail, string password, string recipientEmail, string subject, string body)
        {
            try
            {
                MailMessage mail = new MailMessage();
                mail.From = new MailAddress(senderEmail);
                mail.To.Add(new MailAddress(recipientEmail));
                mail.Subject = subject;
                mail.Body = body;
                mail.IsBodyHtml = true;

                SmtpClient smtpClient = new SmtpClient("smtp.gmail.com", 587);
                smtpClient.EnableSsl = true;
                smtpClient.Credentials = new NetworkCredential(senderEmail, password);

                smtpClient.Send(mail);

                Console.WriteLine("E-mail sent successfully.");
            }
            catch (Exception ex)
            {
                Console.WriteLine("Failed to send e-mail. Error: " + ex.Message);
            }
        }

        enum AccountType
        {
            Person,
            Company
        }

        //  Login Error Message:

        public const int LOGGING_ERROR_MESSAGE_NONE = 0;
        public const int LOGGING_ERROR_MESSAGE_ACCOUNT_NOT_FOUND = 1;
        public const int LOGGING_ERROR_MESSAGE_WRONG_PASSWORD = 2;
        public const int LOGGING_ERROR_MESSAGE_WRONG_REPEATED_PASSWORD = 3;
        public const int LOGGING_ERROR_MESSAGE_NO_EMAIL_AND_PHONE_NUMBER = 4;
        public const int LOGGING_ERROR_MESSAGE_NICKNAME_ALREADY_EXISTS = 5;

        public const int LOGGING_ERROR_MESSAGE_UNKNOWN = -1;

        private readonly IConfiguration _configuration;
        private string _connectionString = "";

        public HomeController(IConfiguration configuration)
        {
            _configuration = configuration;
            _connectionString = _configuration.GetConnectionString("DefaultConnection") ?? "";
        }


        [HttpGet("/")]
        public IActionResult Index()
        {
            var cookieOptions = new CookieOptions
            {
                Secure = true
            };

            DataModel model = new DataModel();
            var session = HttpContext.Session;

            //  isLogged:
            var isLogged = session.GetInt32("isLogged");
            if (isLogged == null)
                session.SetInt32("isLogged", 0);
            else
            {
                model.nickname = session.GetString("nickname") ?? "";
                model.isLogged = (int)isLogged;
            }

            //  Dla dodatkowych informacji o nieudanym logowaniu:
            var loggingErrorMessage = session.GetInt32("loggingErrorMessage");
            if (loggingErrorMessage != null)
            {
                model.loggingErrorMessage = (int)loggingErrorMessage;
                session.Remove("loggingErrorMessage");
            }

            return View(model);
        }

        public bool CheckIfRecordExist(string tableName, params (string columnName, string columnValue)[] conditions)
        {
            bool hasRows = false;

            using (MySqlConnection connection = new MySqlConnection(_connectionString))
            {
                connection.Open();

                string query = $"SELECT * FROM {tableName} WHERE ";
                List<string> whereConditions = new List<string>();
                MySqlCommand command = new MySqlCommand();

                foreach ((string columnName, string columnValue) in conditions)
                {
                    string parameterName = $"@{columnName}";
                    whereConditions.Add($"{columnName} = {parameterName}");
                    command.Parameters.AddWithValue(parameterName, columnValue);
                }

                query += string.Join(" AND ", whereConditions);
                command.CommandText = query;
                command.Connection = connection;

                using (MySqlDataReader reader = command.ExecuteReader())
                {
                    hasRows = reader.HasRows;
                }

                connection.Close();
            }

            return hasRows;
        }

        public int GetMaxId(string tableName)
        {
            int maxId = 0;
            using (MySqlConnection connection = new MySqlConnection(_connectionString))
            {
                connection.Open();

                string query = "SELECT MAX(id) FROM `" + tableName + "`";
                using (MySqlCommand command = new MySqlCommand(query, connection))
                {
                    object result = command.ExecuteScalar()!;
                    if (result != null && result != DBNull.Value)
                        maxId = Convert.ToInt32(result) + 1;
                }

                connection.Close();
            }

            return maxId;
        }

        public bool IsGroupOwner(int id)
        {
            bool result = false;
            using (MySqlConnection connection = new MySqlConnection(_connectionString))
            {
                connection.Open();

                string query = "SELECT `accountFK` FROM `group` WHERE `id` = @id;";
                using (MySqlCommand command = new MySqlCommand(query, connection))
                {
                    command.Parameters.AddWithValue("@id", id);
                    using (var reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            int accountFK = reader.GetInt32("accountFK");
                            result = (HttpContext.Session.GetInt32("id") == accountFK);
                        }
                    }
                }

                connection.Close();
            }

            return result;
        }

        private void logIn(int id, string nickname)
        {
            var session = HttpContext.Session;

            session.SetInt32("isLogged", 1);
            session.SetInt32("id", id);
            session.SetString("nickname", nickname);
        }

        [HttpPost]
        public IActionResult LogInForm(string nickname, string password)
        {
            var session = HttpContext.Session;
            string query = "SELECT * FROM `account` WHERE `nickname` = '" + nickname + "';";

            using (MySqlConnection connection = new MySqlConnection(_connectionString))
            {
                connection.Open();

                if (connection.State == ConnectionState.Open)
                {
                    using (MySqlCommand command = new MySqlCommand(query, connection))
                    {
                        using (MySqlDataReader reader = command.ExecuteReader())
                        {
                            if (reader.Read())
                            {
                                int id = reader.GetInt32(reader.GetOrdinal("id"));
                                string dbPassword = reader.GetString(reader.GetOrdinal("password"));
                                if (dbPassword == password)
                                    logIn(id, nickname);
                                else
                                    session.SetInt32("loggingErrorMessage", LOGGING_ERROR_MESSAGE_WRONG_PASSWORD);
                            }
                            else
                                session.SetInt32("loggingErrorMessage", LOGGING_ERROR_MESSAGE_ACCOUNT_NOT_FOUND);

                            reader.Close();
                        }
                    }

                    connection.Close();
                }
            }

            return RedirectToAction("Index");
        }

        [HttpPost]
        public IActionResult LogOutForm()
        {
            var session = HttpContext.Session;
            session.Remove("id");
            session.Remove("nickname");
            session.SetInt32("isLogged", 0);

            return RedirectToAction("Index");
        }

        private int registerAccount(string nickname, string password, string repeatPassword, string email, string phoneNumber, AccountType type)
        {
            var session = HttpContext.Session;

            //  Źle powtórzone hasło:
            if (password != repeatPassword)
            {
                session.SetInt32("loggingErrorMessage", LOGGING_ERROR_MESSAGE_WRONG_REPEATED_PASSWORD);
                return -1;
            }

            //  Nie podano emailu && telefonu:
            if (string.IsNullOrEmpty(email) && string.IsNullOrEmpty(phoneNumber))
            {
                session.SetInt32("loggingErrorMessage", LOGGING_ERROR_MESSAGE_NO_EMAIL_AND_PHONE_NUMBER);
                return -1;
            }

            using (MySqlConnection connection = new MySqlConnection(_connectionString))
            {
                connection.Open();

                //  Powtórzono nickname:
                string nicknameQuery = "SELECT COUNT(*) FROM account WHERE nickname = @nickname";
                using (MySqlCommand command = new MySqlCommand(nicknameQuery, connection))
                {
                    command.Parameters.AddWithValue("@nickname", nickname);
                    if ((long)command.ExecuteScalar()! != 0)
                    {
                        session.SetInt32("loggingErrorMessage", LOGGING_ERROR_MESSAGE_NICKNAME_ALREADY_EXISTS);
                        connection.Close();
                        return -1;
                    }
                }

                int maxId = GetMaxId("account");

                //  Dodanie konta do bazy danych:
                string addAccountQuery = "INSERT INTO account (id, nickname, password, phoneNumber, email, type) VALUES (@id, @nickname, @password, @phoneNumber, @email, @type)";
                using (MySqlCommand command = new MySqlCommand(addAccountQuery, connection))
                {
                    command.Parameters.AddWithValue("@id", maxId);
                    command.Parameters.AddWithValue("@nickname", nickname);
                    command.Parameters.AddWithValue("@password", password);
                    command.Parameters.AddWithValue("@phoneNumber", phoneNumber);
                    command.Parameters.AddWithValue("@email", email);
                    command.Parameters.AddWithValue("@type", type);

                    command.ExecuteNonQuery();
                }

                connection.Close();
                return maxId;
            }
        }

        [HttpPost]
        public IActionResult RegisterPersonForm(string nickname, string password, string repeatPassword,
            string name, string surname, string email, string phoneNumber)
        {
            int maxId = registerAccount(nickname, password, repeatPassword, email, phoneNumber, AccountType.Person);
            if (maxId != -1)
            {
                using (MySqlConnection connection = new MySqlConnection(_connectionString))
                {
                    connection.Open();

                    string query = "INSERT INTO person (accountFK, name, surname) VALUES (@accountFK, @name, @surname)";
                    using (MySqlCommand command = new MySqlCommand(query, connection))
                    {
                        command.Parameters.AddWithValue("@accountFK", maxId);
                        command.Parameters.AddWithValue("@name", name);
                        command.Parameters.AddWithValue("@surname", surname);

                        command.ExecuteNonQuery();
                    }

                    connection.Close();
                }

                logIn(maxId, nickname);
            }

            return RedirectToAction("Index");
        }

        [HttpPost]
        public IActionResult RegisterCompanyForm(string nickname, string password, string repeatPassword,
            string name, string email, string phoneNumber)
        {
            int maxId = registerAccount(nickname, password, repeatPassword, email, phoneNumber, AccountType.Person);
            if (maxId != -1)
            {
                using (MySqlConnection connection = new MySqlConnection(_connectionString))
                {
                    connection.Open();

                    string query = "INSERT INTO company (accountFK, name) VALUES (@accountFK, @name)";
                    using (MySqlCommand command = new MySqlCommand(query, connection))
                    {
                        command.Parameters.AddWithValue("@accountFK", maxId);
                        command.Parameters.AddWithValue("@name", name);

                        command.ExecuteNonQuery();
                    }

                    connection.Close();
                }

                logIn(maxId, nickname);
            }

            return RedirectToAction("Index");
        }

        [HttpPost]
        public IActionResult SelectGroups()
        {
            HttpContext.Session.SetInt32("contentOptions", (int)ContentOptions.Groups);
            return RedirectToAction("Index");
        }

        [HttpPost]
        public IActionResult GetGroupsPost()
        {
            dynamic data = new {
                groups = new List<dynamic>()
            };

            using (MySqlConnection connection = new MySqlConnection(_connectionString))
            {
                connection.Open();

                string groupQuery = "SELECT `id`, `name` FROM `group` WHERE `accountFK` = '" + HttpContext.Session.GetInt32("id") + "'";
                using (var groupCommand = new MySqlCommand(groupQuery, connection))
                {
                    using (var groupReader = groupCommand.ExecuteReader())
                    {
                        while (groupReader.Read())
                        {
                            int id = groupReader.GetInt32("id");
                            string name = groupReader.GetString("name");

                            dynamic group = new {
                                id = id,
                                name = name,
                            };

                            data.groups.Add(group);
                        }
                    }
                }

                connection.Close();
            }

            return Json(data);
        }

        public IActionResult DeleteGroupsPost([FromBody] int id)
        {
            var result = false;

            var session = HttpContext.Session;
            if (session.GetInt32("isLogged") != 1)
                return Json(result);

            //  Sprawdzenie, czy jest właścicielem grupy:
            if (!IsGroupOwner(id))
                return Json(result);

            //  Usunięcie grupy:
            using (MySqlConnection connection = new MySqlConnection(_connectionString))
            {
                connection.Open();

                string query = "DELETE FROM `group` WHERE `id` = @id;";
                using (MySqlCommand command = new MySqlCommand(query, connection))
                {
                    try
                    {
                        command.Parameters.AddWithValue("@id", id);
                        command.ExecuteNonQuery();
                        result = true;
                    }
                    catch { }
                }

                connection.Close();
            }

            return Json(result);
        }

        public IActionResult AddGroupsPost([FromBody] string name)
        {
            var session = HttpContext.Session;
            if (session.GetInt32("isLogged") != 1)
                return Json(new { result = false });

            int maxId = GetMaxId("group");
            int aid = (int)session.GetInt32("id")!;

            using (MySqlConnection connection = new MySqlConnection(_connectionString))
            {
                connection.Open();

                string query = "INSERT INTO `group` (id, accountFK, name) VALUES (@id, @accountFK, @name)";
                using (MySqlCommand command = new MySqlCommand(query, connection))
                {
                    command.Parameters.AddWithValue("@id", maxId);
                    command.Parameters.AddWithValue("@accountFK", aid);
                    command.Parameters.AddWithValue("@name", name);
                    command.ExecuteNonQuery();
                }

                connection.Close();
            }

            var data = new
            {
                result = true,
                name = name,
                id = maxId
            };

            return Json(data);
        }

        public IActionResult GetAddressesPost([FromBody] int id)
        {
            if (HttpContext.Session.GetInt32("isLogged") != 1)
                return Json(new { result = false });

            if (!IsGroupOwner(id))
                return Json(new { result = false });

            dynamic data = new { result = true, addresses = new List<dynamic>() };
            using (MySqlConnection connection = new MySqlConnection(_connectionString))
            {
                connection.Open();

                string query = "SELECT `email` FROM `address` WHERE `groupFK` = @id;";
                using (var command = new MySqlCommand(query, connection))
                {
                    command.Parameters.AddWithValue("@id", id);
                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                            data.addresses.Add(reader.GetString("email"));
                    }
                }

                connection.Close();
            }

            return Json(data);
        }

        public IActionResult AddAddressPost([FromBody] JsonElement data)
        {
            if (HttpContext.Session.GetInt32("isLogged") != 1)
                return Json(new { result = false });

            string address = data.TryGetProperty("address", out var readAddress) && readAddress.ValueKind == JsonValueKind.String ? readAddress.GetString()! : string.Empty;
            int groupId = data.TryGetProperty("groupId", out var readId) && readId.ValueKind == JsonValueKind.Number ? readId.GetInt32()! : -1;

            //  Sprawdzenie, czy dane są poprawne.
            if (address == string.Empty || groupId == -1)
                return Json(new { result = false });

            //  Sprawdzenie, czy użytkownik jest właścicielem grupy.
            if (!IsGroupOwner(groupId))
                return Json(new { result = false });

            //  Sprawdzenie, czy adres już istnieje?
            if (CheckIfRecordExist("address", ("groupFK", groupId.ToString()), ("email", address)))
                return Json(new { result = false });

            using (MySqlConnection connection = new MySqlConnection(_connectionString))
            {
                connection.Open();

                string query = "INSERT INTO `address` (groupFK, email) VALUES (@groupFK, @email)";
                using (MySqlCommand command = new MySqlCommand(query, connection))
                {
                    command.Parameters.AddWithValue("@groupFK", groupId);
                    command.Parameters.AddWithValue("@email", address);
                    command.ExecuteNonQuery();
                }

                connection.Close();
            }

            return Json(new { result = true });
        }

        public IActionResult DeleteAddressPost([FromBody] JsonElement data)
        {
            if (HttpContext.Session.GetInt32("isLogged") != 1)
                return Json(new { result = false });

            string address = data.TryGetProperty("address", out var readAddress) && readAddress.ValueKind == JsonValueKind.String ? readAddress.GetString()! : string.Empty;
            int groupId = data.TryGetProperty("groupId", out var readId) && readId.ValueKind == JsonValueKind.Number ? readId.GetInt32()! : -1;

            //  Sprawdzenie, czy dane są poprawne.
            if (address == string.Empty || groupId == -1)
                return Json(new { result = false });

            //  Sprawdzenie, czy użytkownik jest właścicielem grupy.
            if (!IsGroupOwner(groupId))
                return Json(new { result = false });

            //  Sprawdzenie, czy adres istnieje.
            if (!CheckIfRecordExist("address", ("groupFK", groupId.ToString()), ("email", address)))
                return Json(new { result = false });

            using (MySqlConnection connection = new MySqlConnection(_connectionString))
            {
                connection.Open();

                string query = "DELETE FROM `address` WHERE `groupFK` = @groupFK AND `email` = @email";
                using (MySqlCommand command = new MySqlCommand(query, connection))
                {
                    command.Parameters.AddWithValue("@groupFK", groupId);
                    command.Parameters.AddWithValue("@email", address);
                    command.ExecuteNonQuery();
                }

                connection.Close();
            }

            return Json(new { result = true });
        }

        public IActionResult sendMailPost([FromBody] JsonElement data)
        {
            if (HttpContext.Session.GetInt32("isLogged") != 1)
                return Json(new { result = false });

            string groupId = data.TryGetProperty("groupId", out var readGroupId) && readGroupId.ValueKind == JsonValueKind.String ? readGroupId.GetString()! : string.Empty;
            string title = data.TryGetProperty("title", out var readTitle) && readTitle.ValueKind == JsonValueKind.String ? readTitle.GetString()! : string.Empty;
            string body = data.TryGetProperty("body", out var readBody) && readBody.ValueKind == JsonValueKind.String ? readBody.GetString()! : string.Empty;
            string email = data.TryGetProperty("email", out var readEmail) && readEmail.ValueKind == JsonValueKind.String ? readEmail.GetString()! : string.Empty;
            string password = data.TryGetProperty("password", out var readPassword) && readPassword.ValueKind == JsonValueKind.String ? readPassword.GetString()! : string.Empty;

            //  Sprawdzenie, czy dane są poprawne.
            if (groupId == string.Empty || title == string.Empty || body == string.Empty || email == string.Empty || password == string.Empty)
                return Json(new { result = false });

            //  Sprawdzenie, czy użytkownik jest właścicielem grupy.
            if (!IsGroupOwner(int.Parse(groupId)))
                return Json(new { result = false });

            using (MySqlConnection connection = new MySqlConnection(_connectionString))
            {
                connection.Open();

                string query = "SELECT `email` FROM `address` WHERE `groupFK` = @groupId;";
                using (MySqlCommand command = new MySqlCommand(query, connection))
                {
                    command.Parameters.AddWithValue("@groupId", groupId);
                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            string address = reader.GetString("email");
                            SendEmail(email, password, address, title, body);
                        }
                    }
                }

                connection.Close();
            }

            return Json(new { result = true });
        }
    }
}